$(document).ready(function () {
    $("#mycarousel").carousel( { interval: 2000 } );
    $("#carousel-button").click(function(){
        if ($("#carousel-button").children("span").hasClass('fa-pause')) {
            $("#mycarousel").carousel('pause');
            $("#carousel-button").removeClass('btn-danger');
            $("#carousel-button").addClass('btn-success');
            $("#carousel-button").children("span").removeClass('fa-pause');
            $("#carousel-button").children("span").addClass('fa-play');
        }
        else if ($("#carousel-button").children("span").hasClass('fa-play')){
            $("#mycarousel").carousel('cycle');
            $("#carousel-button").removeClass('btn-success');
            $("#carousel-button").addClass('btn-danger');
            $("#carousel-button").children("span").removeClass('fa-play');
            $("#carousel-button").children("span").addClass('fa-pause');
        }
    });
});

$(document).ready(function () {
    $("#loginButton").click(function () {
        $("#loginModal").modal();
    });

    $("#login-close").click(function () {
        $("#loginModal").modal('hide');
    });

    $("#reserveButton").click(function() {
        $("#reserveMedal").modal();
    });

    $(".close").click(function() {
        $("#reserveMedal").modal('hide');
    });

    $("#reserve-close").click(function() {
        $("#reserveMedal").modal('hide');
    });
});